import React from "react";
import { useNavigate, useParams } from "react-router-dom";

const UserDelete = ({ eleves }) => {
  let { id } = useParams();
  const navigate = useNavigate();
  const eleve = eleves[id];
  console.log(eleve);
  return (
    <div>
      {
        <ul>
          <li>nom : {eleve.nom}</li>
          <li>prenom : {eleve.prenoms}</li>
          <li>age : {eleve.age} ans</li>
        </ul>
      }

      <button onClick={() => navigate("/")}>Retour à l'accueil</button>
    </div>
  );
};

export default UserDelete;
