import React from "react";
import { useLocation } from "react-router-dom";

const NoPage = () => {
  const location = useLocation();
  console.log(location);
  return (
    <div>
      <h1>404</h1>
    </div>
  );
};

export default NoPage;
