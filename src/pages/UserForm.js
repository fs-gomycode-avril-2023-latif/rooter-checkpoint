import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { add } from "../reduce/reducer/userSlice";

const UserForm = () => {
  const dispatch = useDispatch();

  const [item, setItem] = useState({
    nom: "",
    email: "",
  });

  const ajoutItem = (e) => {
    setItem({ ...item, [e.target.name]: e.target.value });
  };
  const submit = (e) => {
    e.preventDefault();
    dispatch(add(item));
  };
  return (
    <div>
      <h1>UserForm</h1>
      <form action="" onSubmit={submit}>
        <div>
          <label htmlFor="">Nom</label>
          <input onChange={ajoutItem} name="nom" type="text" />
        </div>
        <div>
          <label htmlFor="">Email</label>
          <input name="email" onChange={ajoutItem} type="email" />
        </div>
        <button>Envoyer</button>
      </form>
    </div>
  );
};

export default UserForm;
