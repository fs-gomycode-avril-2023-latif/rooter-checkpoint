import React from "react";
import { useSelector } from "react-redux/es/hooks/useSelector";

const UserList = () => {
  const user = useSelector((state) => state.user.users);
  console.log(user);

  return (
    <div>
      {user.map((item) => (
        <ul>
          <li>{item.nom}</li>
          <li>{item.email}</li>
        </ul>
      ))}
    </div>
  );
};

export default UserList;
