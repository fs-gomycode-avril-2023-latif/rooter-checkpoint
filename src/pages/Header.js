import React from "react";
import { Link, Outlet } from "react-router-dom";

const Header = () => {
  return (
    <>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/UserForm">User Form</Link>
          </li>
          <li>
            <Link to="/UserList">User List</Link>
          </li>
        </ul>
      </nav>

      <Outlet />
    </>
  );
};

export default Header;
