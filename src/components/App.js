import React from "react";
import { Route, Routes } from "react-router-dom";
import Header from "../pages/Header";
import Home from "../pages/Home";
import UserDelete from "../pages/UserDelete";
import UserList from "../pages/UserList";
import UserForm from "../pages/UserForm";
import NoPage from "../pages/NoPage";
import Eleves from "../data/Eleves";

const App = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Header />}>
          <Route index element={<Home />} />
          <Route
            path="UserDelete/:id"
            element={<UserDelete eleves={Eleves} />}
          />
          <Route path="UserList" element={<UserList />} />
          <Route path="UserForm" element={<UserForm />} />
          <Route path="*" element={<NoPage />} />
        </Route>
      </Routes>
    </div>
  );
};

export default App;
