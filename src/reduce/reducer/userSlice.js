import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: {},
  users: [],
};

export const userSlice = createSlice({
  name: "counter",
  initialState,
  reducers: {
    add: (state, action) => {
      state.users.push(action.payload);
    },
    edit: (state, action) => {
      state.user = action.payload;
    },
    destroy: (state, action) => {
      state.users = state.users.filter((user) => user.id != action.payload);
    },
  },
});

// Action creators are generated for each case reducer function
export const { add, edit, destroy } = userSlice.actions;

export default userSlice.reducer;
